<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherData extends Model
{
    use HasFactory;
    protected $table = 'weather_data';
    protected $fillable = ['temperature', 'is_rain', 'sensor_id'];
}
