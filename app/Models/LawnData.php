<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawnData extends Model
{
    use HasFactory;
    protected $table = 'lawn_data';
    protected $fillable = ['ph_value', 'fert_date', 'is_need_water', 'sensor_id', 'username'];

}
