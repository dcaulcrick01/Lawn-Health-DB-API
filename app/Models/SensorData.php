<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{
    use HasFactory;
    protected $table = 'sensor_data';
    protected $fillable = ['ph_value', 'air_temparature', 'air_humidity', 'soil_moisture_value', 'battery_life', 'sensor_id', 'username'];
}
