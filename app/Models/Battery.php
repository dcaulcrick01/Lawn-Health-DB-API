<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battery extends Model
{
    use HasFactory;

    protected $table = 'battery_life';

    protected $fillable = [
        'value',
        'sensor_id'
    ];
}
