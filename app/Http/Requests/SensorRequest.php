<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SensorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = "/^\d+(\.\d{1,2})?$/";
        return [
            'sensor_id' => 'required|integer',
            'username' => 'string',
            'ph_value' => 'required|regex:'.$regex,
            'air_temparature' => 'required|regex:'.$regex,
            'air_humidity' => 'required|regex:'.$regex,
            'soil_moisture_value' => 'required|regex:'.$regex,
            'battery_life'=> 'required|regex:'.$regex,
        ];
    }
}
