<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LawnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sensor_id' => 'required|integer',
            'username' => 'required|string',
            'fert_date' => 'required|date',
            'is_need_water' => 'required|boolean',
        ];
    }
}
