<?php

namespace App\Http\Controllers;

use App\Http\Requests\LawnRequest;
use App\Models\LawnData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class LawnDataController extends Controller
{
    public function userData($id)
    {
        try {
            $resource = LawnData::where('sensor_id', $id)->get();
            return Response::json([
                'success' => true,
                'message' => 'Lawn data found!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error getting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $resource = LawnData::all();
            return Response::json([
                'success' => true,
                'message' => 'Lawn data found!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error getting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LawnRequest $request)
    {
        try {
            $resource = LawnData::create($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Lawn item created!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error saving data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LawnData  $lawnData
     * @return \Illuminate\Http\Response
     */
    public function show(LawnData $lawnData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LawnData  $lawnData
     * @return \Illuminate\Http\Response
     */
    public function edit(LawnData $lawnData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LawnData  $lawnData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LawnData $lawnData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LawnData  $lawnData
     * @return \Illuminate\Http\Response
     */
    public function destroy(LawnData $lawnData)
    {
        try {
            $lawnData->delete();
            return Response::json([
                'success' => true,
                'message' => 'Lawn item deleted!',
                'data' => $lawnData
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error deleting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}
