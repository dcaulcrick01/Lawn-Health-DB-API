<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeatherRequest;
use App\Models\User;
use App\Models\WeatherData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class WeatherDataController extends Controller
{
    public function userData($username)
    {
        $userExist = User::where('name', $username)->first();
        if($userExist){
            $resource = WeatherData::all();
            return Response::json([
                'success' => true,
                'message' => 'Weather data found!',
                'data' => $resource
            ], 200);
        } else {
            return Response::json([
                'success' => true,
                'message' => 'User does not exist',
                'data' => null
            ], 500);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $resource = WeatherData::all();
            return Response::json([
                'success' => true,
                'message' => 'Weather data found!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error getting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WeatherRequest $request)
    {
        try {
            $resource = WeatherData::create($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Weather item created!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error saving data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WeatherData  $weatherData
     * @return \Illuminate\Http\Response
     */
    public function show(WeatherData $weatherData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WeatherData  $weatherData
     * @return \Illuminate\Http\Response
     */
    public function edit(WeatherData $weatherData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WeatherData  $weatherData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeatherData $weatherData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WeatherData  $weatherData
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeatherData $weatherData)
    {
        try {
            $weatherData->delete();
            return Response::json([
                'success' => true,
                'message' => 'Weather item deleted!',
                'data' => $weatherData
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error deleting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}
