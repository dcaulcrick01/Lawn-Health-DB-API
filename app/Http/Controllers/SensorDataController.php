<?php

namespace App\Http\Controllers;

use App\Http\Requests\SensorRequest;
use App\Models\SensorData;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SensorDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function customUpdate(Request $request, $sensor_id)
    {
        $sensor = SensorData::where('sensor_id', $sensor_id)->first();
        if ($sensor) {
            $sensor->update($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Sensor data updated successful!',
                'data' => $sensor
            ], 200);
        } else {
            return Response::json([
                'success' => true,
                'message' => 'Sensor data not found!',
                'data' => null
            ], 200);
        }
    }

    public function userData($username)
    {
        $userExist = User::where('name', $username)->get();
        if ($userExist) {
            $resource = SensorData::where('username', $username)->orderBy('created_at', 'desc')->get();
            return Response::json([
                'success' => true,
                'message' => 'Sensor data found!',
                'data' => $resource
            ], 200);
        } else {
            return Response::json([
                'success' => true,
                'message' => 'There was an error getting data, user does not exist',
                'data' => null
            ], 500);
        }
    }


    public function index()
    {
        try {
            $resource = SensorData::all();
            return Response::json([
                'success' => true,
                'message' => 'Sensor data found!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error getting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SensorRequest $request)
    {
        $sensor = SensorData::where('sensor_id', $request->sensor_id)->first();
        if ($sensor) {
            $sensor->update($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Sensor data updated successful!',
                'data' => $sensor
            ], 200);
        } else {
            try {
                $resource = SensorData::create($request->all());
                return Response::json([
                    'success' => true,
                    'message' => 'Sensor item created!',
                    'data' => $resource
                ], 200);
            } catch (\Throwable $th) {
                return Response::json([
                    'success' => true,
                    'message' => 'There was an error saving data',
                    'data' => $th->getMessage()
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SensorData  $sensorData
     * @return \Illuminate\Http\Response
     */
    public function show(SensorData $sensorData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SensorData  $sensorData
     * @return \Illuminate\Http\Response
     */
    public function edit(SensorData $sensorData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SensorData  $sensorData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $sensorData =  SensorData::findOrFail($id);
        try {
            $resource = $sensorData->update($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Sensor item updated!',
                'data' => $sensorData
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error saving data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SensorData  $sensorData
     * @return \Illuminate\Http\Response
     */
    public function destroy(SensorData $sensorData)
    {
        try {
            $sensorData->delete();
            return Response::json([
                'success' => true,
                'message' => 'Sensor item deleted!',
                'data' => $sensorData
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error deleting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}
