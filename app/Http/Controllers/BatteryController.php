<?php

namespace App\Http\Controllers;

use App\Http\Requests\BatteryRequest;
use App\Models\Battery;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BatteryController extends Controller
{

    public function userData($username)
    {
        $userExist = User::where('name', $username)->first();
        if($userExist){
            $resource = Battery::all();
            return Response::json([
                'success' => true,
                'message' => 'Battery data found!',
                'data' => $resource
            ], 200);
        } else {
            return Response::json([
                'success' => true,
                'message' => 'User does not exist',
                'data' => null
            ], 500);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json([
            'success' => true,
            'message' => 'Weather data found!',
            'data' => Battery::all()
        ], 200);
    }

    public function store(BatteryRequest $request)
    {
        try {
            $resource = Battery::create($request->all());
            return Response::json([
                'success' => true,
                'message' => 'Item created!',
                'data' => $resource
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error saving data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Battery::findOrFail($id);
        try {
            $data->delete();
            return Response::json([
                'success' => true,
                'message' => 'Item deleted!',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return Response::json([
                'success' => true,
                'message' => 'There was an error deleting data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}
