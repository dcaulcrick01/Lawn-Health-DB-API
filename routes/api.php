<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BatteryController;
use App\Http\Controllers\LawnDataController;
use App\Http\Controllers\SensorDataController;
use App\Http\Controllers\WeatherDataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use phpDocumentor\Reflection\Types\Resource_;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register']);

Route::resource('sensor', SensorDataController::class);


Route::post('sensor-update/{sensor_id}', [SensorDataController::class, 'customUpdate']);
Route::get('sensor-list/{username}', [SensorDataController::class, 'userData']);

Route::resource('weather', WeatherDataController::class);
Route::get('weather-list/{username}', [WeatherDataController::class, 'userData']);

Route::resource('battery-life', BatteryController::class);
Route::get('battery-life-list/{username}', [BatteryController::class, 'userData']);

Route::resource('lawn', LawnDataController::class);
Route::get('lawn-list/{username}', [LawnDataController::class, 'userData']);
